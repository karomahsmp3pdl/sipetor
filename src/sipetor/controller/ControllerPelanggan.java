/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sipetor.controller;

import java.util.List;
import sipetor.model.Pelanggan;
import sipetor.query.QueryPelanggan;

/**
 *
 * @author MK Almunawar
 */
public class ControllerPelanggan {
    private QueryPelanggan queryPelanggan = new  QueryPelanggan();
    
    public List<Pelanggan> getAllData() {
        return queryPelanggan.getAllPelanggan();
    }
    
    public List<Pelanggan> getAllPelangganByName(String nama_pelanggan) {
        return queryPelanggan.getAllPelangganByName(nama_pelanggan);
    }
    
    public boolean insertPelanggan(Pelanggan p){
        return queryPelanggan.insert(p);
    }
    
    public boolean updatePelanggan(Pelanggan p){
        return queryPelanggan.update(p);
    }
    
    public boolean deletePelanggan(String tnkb){
        return queryPelanggan.delete(tnkb);
    }

}
