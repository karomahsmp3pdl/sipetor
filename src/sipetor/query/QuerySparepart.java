/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sipetor.query;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import sipetor.connection.SQLConnection;
import sipetor.model.Sparepart;
import sipetor.model.inteface.InterfaceSparepart;

/**
 *
 * @author MK Almunawar
 */
public class QuerySparepart implements InterfaceSparepart{
    Connection conn = SQLConnection.getConnection();
    
    @Override
    public boolean insert(Sparepart data) {
        try {
            String sql = "INSERT INTO sparepart VALUES(?, ?, ?, ?)";
            
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setByte(1, data.getId_sparepart());
            statement.setString(2, data.getNama_sparepart());
            statement.setInt(3, data.getHarga_sparepart());
            statement.setInt(4, data.getStock_sparepart());
            int row = statement.executeUpdate();
            if (row > 0) {
                return true;
            }
            statement.close();
        } catch (SQLException ex) {
            Logger.getLogger(QuerySparepart.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean update(Sparepart data) {
        try {
            String sql = "UPDATE sparepart SET nama_sparepart=?, harga_sparepart=?,"
                    + "stock_sparepart=? WHERE id_sparepart=?";
            
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, data.getNama_sparepart());
            statement.setInt(2, data.getHarga_sparepart());
            statement.setInt(3, data.getStock_sparepart());
            statement.setByte(4, data.getId_sparepart());
            int row = statement.executeUpdate();
            if (row > 0) {
                return true;
            }
            statement.close();
        } catch (SQLException ex) {
            Logger.getLogger(QuerySparepart.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean delete(byte id_sparepart) {
      try {
            String sql = "DELETE FROM sparepart WHERE id_sparepart=?";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setByte(1, id_sparepart);
            int row = statement.executeUpdate();
            if (row > 0) {
                return true;
            }
            statement.close();
        } catch (SQLException ex) {
            Logger.getLogger(QuerySparepart.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;  
    }

    @Override
    public int generateID() {
        int id_golongan = 0;
        String sql = "EXEC genIDSparepart";
        try {
            PreparedStatement statement = conn.prepareStatement(sql);
            
            ResultSet rs = statement.executeQuery();
            while (rs.next()){
                id_golongan = rs.getInt(1);
            }
            statement.close();
        } catch (SQLException ex) {
            Logger.getLogger(Sparepart.class.getName()).log(Level.SEVERE, null, ex);
        }
        return id_golongan;
    }

    @Override
    public List<Sparepart> getAllSparepart() {
        List<Sparepart> list = new ArrayList<>();
        String sql = "SELECT * FROM sparepart";
        if (SQLConnection.getConnection() == null) {
            return null;
        } else {
            try {
                PreparedStatement statement = conn.prepareStatement(sql);
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    Sparepart data = new Sparepart(rs.getByte(1),
                            rs.getString(2),
                            rs.getInt(3),
                            rs.getInt(4));
                    list.add(data);
                }
                statement.close();
            } catch (SQLException ex) {
                Logger.getLogger(QuerySparepart.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return list;
    }

    @Override
    public Sparepart getOneSparepartByID(byte id_sparepart) {
        Sparepart output = null;
        String sql = "SELECT * FROM sparepart WHERE id_sparepart=?";
        if (SQLConnection.getConnection() == null) {
            return null;
        } else {
            try {
                PreparedStatement statement = conn.prepareStatement(sql);
                statement.setByte(1, id_sparepart);
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    output = new Sparepart(rs.getByte(1),
                            rs.getString(2),
                            rs.getInt(3),
                            rs.getInt(4));
                }
                statement.close();
            } catch (SQLException ex) {
                Logger.getLogger(QuerySparepart.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return output;
    }

    @Override
    public List<Sparepart> getAllSparepartByName(String nama_sparepart) {
        List<Sparepart> list = new ArrayList<>();
        String sql = "SELECT * FROM sparepart WHERE nama_sparepart like '%"+nama_sparepart+"%'";
        if (SQLConnection.getConnection() == null) {
            return null;
        } else {
            try {
                PreparedStatement statement = conn.prepareStatement(sql);
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    Sparepart data = new Sparepart(rs.getByte(1),
                            rs.getString(2),
                            rs.getInt(3),
                            rs.getInt(4));
                    list.add(data);
                }
                statement.close();
            } catch (SQLException ex) {
                Logger.getLogger(QuerySparepart.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return list;
    }
    
}
