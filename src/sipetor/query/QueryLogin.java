/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sipetor.query;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import sipetor.connection.SQLConnection;
import sipetor.model.Login;
import sipetor.model.inteface.InterfaceLogin;

/**
 *
 * @author MK Almunawar
 */
public class QueryLogin implements InterfaceLogin {
    private Connection conn = SQLConnection.getConnection();

    @Override
    public boolean insert(Login data) {
        try {
            String sql = "INSERT INTO login values(?,?,?)" ;
            
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, data.getUsername());
            statement.setString(2, data.getPassword());
            statement.setString(3, data.getNik());
            int row = statement.executeUpdate();
            if (row > 0) {
                return true;
            }
            statement.close();
        } catch (SQLException ex) {
            Logger.getLogger(QueryLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    //digunakan buat mendelete pada tabel karyawan
    @Override
    public boolean delete(String nik) {
        try {
            String sql = "DELETE FROM login where nik=?";
            
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, nik);
            int row = statement.executeUpdate();
            if (row > 0) {
                return true;
            }
            statement.close();
        } catch (SQLException ex) {
            Logger.getLogger(QueryLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    //untuk mengambil data table untuk login
    @Override
    public String isCorrect (String username, String password) {
        String nama_karyawan = null;
        String sql = "select a.*, b.nama_karyawan"
                + " FROM login a INNER JOIN karyawan b"
                + " ON a.nik = b.nik"
                + " WHERE a.username = ? AND a.password Like ?";
        if (SQLConnection.getConnection()==null) {
            return nama_karyawan;
        }else {
            try {
                PreparedStatement statement = conn.prepareStatement(sql);
                statement.setString(1, username);
                statement.setString(2, password);
                
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    nama_karyawan = rs.getString(4);
                    return nama_karyawan;
                }
                statement.close();
            } catch (SQLException ex) {
                Logger.getLogger(QueryLogin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return nama_karyawan;
    }

    @Override
    public boolean update(Login data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
